package com.thg.accelerator;

public class Board {
    private Piece[][] gameboard = new Piece[3][3];


    public Board() {
        for (int i = 0; i < gameboard.length;i++){
            for (int j = 0; j < gameboard.length; j++){
                this.gameboard[i][j] = Piece.NOTHING;
            }
        }
    }

    public void visualise(){
        for (int i = 0; i < gameboard.length;i++){
            System.out.println("");
            for (int j = 0; j < gameboard.length; j++){
                System.out.print(" " + Piece.Format((this.gameboard[i][j]))+ " ");
            }
        }
    }



    public void updateBoard(int x, int y, Player player){
        if (x >=  3 || y >= 3){
            System.out.println("coordinates out of range, get rekt you miss a turn");
        }

        if (this.getGameboard()[y][x] != Piece.NOTHING){
            System.out.println("Piece already here");
        }
        else{
            this.gameboard[y][x] = player.getPiece();
        }
    }

    public boolean winCheck(Piece piece){
        if (this.threeInARow(piece)){
            return true;
        }
        else{
            return false;
        }
    }

    public boolean threeInARow(Piece piece){
        int countAcross = 0;
        int countDown = 0;
        int countMainDiagnol = 0;
        int countOffDiagnol = 0;
        for (int i = 0; i < gameboard.length;i++){
            for (int j = 0; j < gameboard.length; j++){
                if (this.getGameboard()[i][j] == piece){
                    countAcross += 1;
                }
                if (this.getGameboard()[j][i] == piece) {
                    countDown += 1;
                }
            }
            if (this.getGameboard()[i][i] == piece){
                countMainDiagnol += 1;
            }
            if (this.getGameboard()[gameboard.length - i - 1][i] == piece){
                countOffDiagnol += 1;
            }
            if (countAcross == gameboard.length || countDown == gameboard.length || countMainDiagnol == gameboard.length || countOffDiagnol == gameboard.length){
                return true;
            }
            countAcross = 0;
            countDown = 0;
        }
        return false;
    }



    public Piece[][] getGameboard() {
        return gameboard;
    }

}
