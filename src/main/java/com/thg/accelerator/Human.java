package com.thg.accelerator;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Human extends Player {

    Human(Piece piece) {
        super(piece);
    }


    public Map<String, Integer> playPiece(Board gameboard) {
        System.out.println("select coordinates:");
        Scanner scanner = new Scanner(System.in);
        System.out.print("x:");
        int x = scanner.nextInt();
        System.out.print("y:");
        int y = scanner.nextInt();
        Map<String, Integer> coordinates = new HashMap<>();
        coordinates.put("x",x);
        coordinates.put("y",y);



        return coordinates;

    }
}
