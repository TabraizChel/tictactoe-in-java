package com.thg.accelerator;

public enum Piece {
     NOUGHT,
     CROSS ,
    NOTHING;

    public static String Format(Piece piece){
        switch (piece){
            case CROSS: return "X";

            case NOUGHT: return "O";

            case NOTHING: return ".";

        }
        return "";
    }
}
