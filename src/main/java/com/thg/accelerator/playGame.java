package com.thg.accelerator;

import java.util.Map;

public class playGame {
    private Board board;
    private Player player1;
    private Player player2;



    public playGame() {
        Board board = new Board();
        Player player1 = new Human(Piece.CROSS);
        Player player2 = new AI(Piece.NOUGHT);
        this.board = board;
        this.player1 = player1;
        this.player2 = player2;
    }

    public void iniate (){
        this.player1.setPiece(Piece.CROSS);
        this.player2.setPiece(Piece.NOUGHT);
        this.player1.setTurn(true);


        while(true){
            System.out.println();
            System.out.println(this.turnChecker().getPiece() + "'s turn, select a coordinate you want to play:");
            Map<String, Integer> coordinatesToPlay = this.turnChecker().playPiece(this.getBoard());
            board.updateBoard(coordinatesToPlay.get("x"), coordinatesToPlay.get("y"), this.turnChecker());
            this.getBoard().visualise();
            System.out.println();
            if (this.getBoard().winCheck(this.turnChecker().getPiece())){
                System.out.println(this.turnChecker().getPiece() + "'s won the game !!");
                break;
            }

            this.changeTurn(this.getPlayer1(), this.getPlayer2());
        }
    }

    private Player turnChecker(){
        if (this.player1.isTurn()){
            return this.player1;
        }
        else{
            return this.player2;
        }
    }

    private void changeTurn(Player player1, Player player2){
        player1.changeTurn();
        player2.changeTurn();
    }


    private Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    private Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    private Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }
}
