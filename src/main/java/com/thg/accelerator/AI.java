package com.thg.accelerator;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class AI extends Player {

    public AI(Piece piece) {
        super(piece);
    }


    public Map<String, Integer> playPiece(Board gameboard) {
        Map<String, Integer> coordinatesOffense = offensivePlay(gameboard);
        Map<String, Integer> coordinatesDefense = defensivePlay(gameboard);
        Map<String, Integer> coordinates = new HashMap<>();


        if (!coordinatesOffense.isEmpty()){
            coordinates = coordinatesOffense;
            return coordinates;
        }

        else if (!coordinatesDefense.isEmpty()){
            coordinates = coordinatesDefense;
            return coordinates;
        }

        else{
            coordinates.put("x",0);
            coordinates.put("y",0);
        }
        return coordinates;
    }


    public Map<String, Integer> offensivePlay(Board gameboard){
        Map<String, Integer> coordinates = new HashMap<>();
        Map<String, Integer> coordinatesAcross = PositionsAcross(gameboard);
        Map<String, Integer> coordinatesDown = PositionsDown(gameboard);
        Map<String, Integer> coordinatesDiagnol = PositionsDiagnol(gameboard);
        Map<String, Integer> coordinatesDiagnolOff = PositionsDiagnolOff(gameboard);
        if (!coordinatesAcross.isEmpty()){
            coordinates = coordinatesAcross;
        }
        else if (!coordinatesDown.isEmpty()){
            coordinates=coordinatesDown;
        }
        else if (!coordinatesDiagnol.isEmpty()){
            coordinates=coordinatesDiagnol;
        }
        else if (!coordinatesDiagnolOff.isEmpty()){
            coordinates=coordinatesDiagnolOff;
        }

        return coordinates;
    }

    public Map<String, Integer> defensivePlay(Board gameboard){
        Map<String, Integer> coordinates = new HashMap<>();
        Map<String, Integer> coordinatesAcross = PositionsAcrossDefense(gameboard);
        Map<String, Integer> coordinatesDown = PositionsDownDefense(gameboard);
        Map<String, Integer> coordinatesDiagnol = PositionsDiagnolDefense(gameboard);
        Map<String, Integer> coordinatesDiagnolOff = PositionsDiagnolOffDefense(gameboard);
        if (!coordinatesAcross.isEmpty()){
            coordinates = coordinatesAcross;
        }
        else if (!coordinatesDown.isEmpty()){
            coordinates=coordinatesDown;
        }
        else if (!coordinatesDiagnol.isEmpty()){
            coordinates=coordinatesDiagnol;
        }
        else if (!coordinatesDiagnolOff.isEmpty()){
            coordinates=coordinatesDiagnolOff;
        }

        return coordinates;
    }


    private Map<String, Integer> PositionsAcross(Board gameboard){
        int countAcross = 0;
        Map<String, Integer> coordinates = new HashMap<>();
        for (int i = 0; i < gameboard.getGameboard().length;i++){
            for (int j = 0; j < gameboard.getGameboard().length; j++){
                if (gameboard.getGameboard()[i][j] == this.getPiece()){
                    countAcross += 1;
                }
                if (countAcross == gameboard.getGameboard().length -1){
                    countAcross = 0;
                    for (int k = 0; k < gameboard.getGameboard().length; k++){
                        if (gameboard.getGameboard()[i][k] == Piece.NOTHING){
                            coordinates.put("x",k);
                            coordinates.put("y",i);
                        }
                    }
                }
            }

        }
        return coordinates;

    }

    private Map<String, Integer> PositionsDown(Board gameboard){
        int countDown = 0;
        Map<String, Integer> coordinates = new HashMap<>();
        for (int i = 0; i < gameboard.getGameboard().length;i++){
            for (int j = 0; j < gameboard.getGameboard().length; j++){
                if (gameboard.getGameboard()[j][i] == this.getPiece()){
                    countDown += 1;
                }
                if (countDown == gameboard.getGameboard().length -1){
                    countDown = 0;
                    for (int k = 0; k < gameboard.getGameboard().length; k++){
                        if (gameboard.getGameboard()[k][i] == Piece.NOTHING){
                            coordinates.put("x",i);
                            coordinates.put("y",k);
                        }
                    }
                }
            }
        }
        return coordinates;

    }

    private Map<String, Integer> PositionsDiagnol(Board gameboard){
        int countDiagnol = 0;
        Map<String, Integer> coordinates = new HashMap<>();
        for (int i = 0; i < gameboard.getGameboard().length;i++){
            if (gameboard.getGameboard()[i][i] == this.getPiece()){
                countDiagnol += 1;
            }
            if (countDiagnol == gameboard.getGameboard().length -1){
                for (int j = 0; j < gameboard.getGameboard().length; j++){
                    if (gameboard.getGameboard()[j][j] == Piece.NOTHING){
                        coordinates.put("x",j);
                        coordinates.put("y",j);
                    }
                }
            }

        }
        return coordinates;

    }

    private Map<String, Integer> PositionsDiagnolOff(Board gameboard){
        int countDiagnolOff = 0;
        Map<String, Integer> coordinates = new HashMap<>();
        for (int i = 0; i < gameboard.getGameboard().length;i++){
            if (gameboard.getGameboard()[gameboard.getGameboard().length - i - 1][i] == this.getPiece()){
                countDiagnolOff += 1;
            }
            if (countDiagnolOff == gameboard.getGameboard().length -1){
                for (int j = 0; j < gameboard.getGameboard().length; j++){
                    if (gameboard.getGameboard()[gameboard.getGameboard().length - j - 1][j] == Piece.NOTHING){
                        coordinates.put("x",j);
                        coordinates.put("y",gameboard.getGameboard().length - j - 1);
                    }
                }
            }

        }
        return coordinates;

    }

    private Map<String, Integer> PositionsAcrossDefense(Board gameboard){
        int countAcross = 0;
        Map<String, Integer> coordinates = new HashMap<>();
        for (int i = 0; i < gameboard.getGameboard().length;i++){
            for (int j = 0; j < gameboard.getGameboard().length; j++){
                if (gameboard.getGameboard()[i][j] != this.getPiece() && gameboard.getGameboard()[i][j] != Piece.NOTHING){
                    countAcross += 1;
                }
                if (countAcross == gameboard.getGameboard().length -1){
                    countAcross = 0;
                    for (int k = 0; k < gameboard.getGameboard().length; k++){
                        if (gameboard.getGameboard()[i][k] == Piece.NOTHING){
                            coordinates.put("x",k);
                            coordinates.put("y",i);
                        }
                    }
                }
            }

        }
        return coordinates;

    }

    private Map<String, Integer> PositionsDownDefense(Board gameboard){
        int countDown = 0;
        Map<String, Integer> coordinates = new HashMap<>();
        for (int i = 0; i < gameboard.getGameboard().length;i++){
            for (int j = 0; j < gameboard.getGameboard().length; j++){
                if (gameboard.getGameboard()[j][i] != this.getPiece() && gameboard.getGameboard()[j][i] != Piece.NOTHING){
                    countDown += 1;
                }
                if (countDown == gameboard.getGameboard().length -1){
                    countDown = 0;
                    for (int k = 0; k < gameboard.getGameboard().length; k++){
                        if (gameboard.getGameboard()[k][i] == Piece.NOTHING){
                            coordinates.put("x",i);
                            coordinates.put("y",k);
                        }
                    }
                }
            }
        }
        return coordinates;

    }

    private Map<String, Integer> PositionsDiagnolDefense(Board gameboard){
        int countDiagnol = 0;
        Map<String, Integer> coordinates = new HashMap<>();
        for (int i = 0; i < gameboard.getGameboard().length;i++){
            if (gameboard.getGameboard()[i][i] == this.getPiece() && gameboard.getGameboard()[i][i] != Piece.NOTHING){
                countDiagnol += 1;
            }
            if (countDiagnol == gameboard.getGameboard().length -1){
                for (int j = 0; j < gameboard.getGameboard().length; j++){
                    if (gameboard.getGameboard()[j][j] == Piece.NOTHING){
                        coordinates.put("x",j);
                        coordinates.put("y",j);
                    }
                }
            }

        }
        return coordinates;

    }

    private Map<String, Integer> PositionsDiagnolOffDefense(Board gameboard){
        int countDiagnolOff = 0;
        Map<String, Integer> coordinates = new HashMap<>();
        for (int i = 0; i < gameboard.getGameboard().length;i++){
            if (gameboard.getGameboard()[gameboard.getGameboard().length - i - 1][i] == this.getPiece() && gameboard.getGameboard()[gameboard.getGameboard().length - i - 1][i] != Piece.NOTHING){
                countDiagnolOff += 1;
            }
            if (countDiagnolOff == gameboard.getGameboard().length -1){
                for (int j = 0; j < gameboard.getGameboard().length; j++){
                    if (gameboard.getGameboard()[gameboard.getGameboard().length - j - 1][j] == Piece.NOTHING){
                        coordinates.put("x",j);
                        coordinates.put("y",gameboard.getGameboard().length - j - 1);
                    }
                }
            }

        }
        return coordinates;

    }


}
