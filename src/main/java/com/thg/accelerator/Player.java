package com.thg.accelerator;
import java.util.Map;

public abstract class Player {
    private Piece piece;
    private boolean turn;

    public Player(Piece piece) {
        this.piece = piece;
        this.turn = false;
    }

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public boolean isTurn() {
        return turn;
    }

    public void setTurn(boolean turn) {
        this.turn = turn;
    }

    abstract Map<String, Integer> playPiece(Board gameboard);


    public void changeTurn(){
        if (this.turn){
            this.turn = false;
        }
        else{
            this.turn = true;
        }
    }
}
